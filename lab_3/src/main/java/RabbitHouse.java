import java.io.InputStreamReader;
import java.util.Scanner;

public class RabbitHouse {
    public static int recursive(int length, int prev) {
        //Base case
        if (length == 1) {
            return 1;
        }
        //Recursive case
        else {
            return length*prev + recursive(length-1, length*prev);
        }
    }

    /*
    Helper hanya untuk initiate rekursif
     */
    public static int helper(String s) {
        return recursive(s.length(), 1);
    }

    public static void main(String[] args) {
        Scanner s = new Scanner(new InputStreamReader(System.in));
        String in = s.nextLine();
        String[] input = in.split(" ");
        System.out.println(helper(input[1]));
    }
}
