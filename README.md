# Repo Buat Kamu, Iya Kamu
# Repository Tutorial & Lab
Dasar-dasar Pemrograman 2 - CSGE601021 | Fakultas Ilmu Komputer, Universitas Indonesia, Semester Genap 2017/2018
***


## Daftar Isi

Repository ini akan berisi materi-materi Tutorial & Lab DDP 2.

1. Lab
    1. [Lab 1](https://gitlab.com/DDP2-CSUI/ddp-lab/blob/master/lab_instructions/lab_1/README.md) - Pengenalan Java & Git
    2. [Lab 2](https://gitlab.com/DDP2-CSUI/ddp-lab/blob/master/lab_instructions/lab_2/README.md) - Konsep Dasar Pemrograman Java
    3. [Lab 3](https://gitlab.com/DDP2-CSUI/ddp-lab/blob/master/lab_instructions/lab_3/README.md) - Rekursif
    4. [Lab 4](https://gitlab.com/DDP2-CSUI/ddp-lab/blob/master/lab_instructions/lab_4/README.md) - Object Oriented Programming
    5. [Lab 5](https://gitlab.com/DDP2-CSUI/ddp-lab/blob/master/lab_instructions/lab_5/README.md) - Array dan ArrayList

***

_Tools_ yang akan digunakan dalam mata kuliah ini antara lain :

- Java Development Kit (JDK) 8
- Git
- Notepad++ (atau text editor sejenisnya)
- Integrated Development Environment (IDE)
- Gradle
- GitLab Account

Pastikan kalian telah menginstall / memiliki _tools_ diatas, jika belum bisa melihat petunjuk penginstallan dan
konfigurasinya [disini](https://drive.google.com/file/d/1c1AA-9ju1S82-NYyV7EMyPNwScPpMQsr/view?usp=sharing)

Kontak Informasi :

- Line Dek Depe : [@nhz2170m](https://line.me/R/ti/p/%40nhz2170m)
>>>>>>> b5e2257e13c6e5d881dc6d257effa5eeb7e94eb2
