import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        Number[][] numbers = new Number[5][5];
        String masukan;
        for(int i = 0; i < 5; i++) {
            masukan = s.nextLine();
            String[] nums = masukan.split(" ");
            for(int j = 0; j < 5; j++) {
                Number num = new Number(Integer.parseInt(nums[j]), i, j);
                numbers[i][j] = num;
            }
        }
        BingoCard bingoCard = new BingoCard(numbers);
        while (!bingoCard.isBingo()) {
            masukan = s.nextLine();
            String[] perintah = masukan.split(" ");
            if(perintah[0].equals("MARK")) {
                System.out.println(bingoCard.markNum(Integer.parseInt(perintah[1])));
                if(bingoCard.isBingo()) {
                    System.out.println(bingoCard.info());
                }
            }
            else if(perintah[0].equals("INFO")) {
                System.out.println(bingoCard.info());
            }
            else if(perintah[0].equals("RESTART")) {
                bingoCard.restart();
            }
        }
    }
}
