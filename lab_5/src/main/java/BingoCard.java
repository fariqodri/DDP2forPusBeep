/**
 * BingoCard Template created by Nathaniel Nicholas
 * Template untuk mengerjakan soal bonus tutorial lab 5
 * Template ini tidak wajib digunakan
 * Side Note : Jangan lupa untuk membuat class baru yang memiliki method main untuk menjalankan program dengan spesifikasi yang diharapkan
 */

public class BingoCard {

	private Number[][] numbers;
	private Number[] numberStates; 
	private boolean isBingo;
	
	public BingoCard(Number[][] numbers) {
		this.numbers = numbers;
		this.isBingo = false;
	}

	public Number[][] getNumbers() {
		return numbers;
	}

	public void setNumbers(Number[][] numbers) {
		this.numbers = numbers;
	}

	public Number[] getNumberStates() {
		return numberStates;
	}

	public void setNumberStates(Number[] numberStates) {
		this.numberStates = numberStates;
	}

	private boolean checkHorizontal(int x) {
		for(int i = 0; i < numbers[x].length; i++) {
			if(!numbers[x][i].isChecked()) {
				return false;
			}
		}
		return true;
	}

	private boolean checkVertical(int y) {
		for(int i = 0; i < numbers.length; i++) {
			if(!numbers[i][y].isChecked()) {
				return false;
			}
		}
		return true;
	}

	private boolean checkLeftToRightDiagonal() {
		for(int i = 0; i < numbers.length; i++) {
			if(!numbers[i][i].isChecked()) {
				return false;
			}
		}
		return true;
	}

	private boolean checkRightToLeftDiagonal() {
		int count = numbers.length - 1;
		for(int i = numbers.length - 1; i >= 0; i--) {
			if(!numbers[count - i][i].isChecked()) {
				return false;
			};
		}
		return true;
	}

	private boolean isBingo(int x, int y) {
		if(checkHorizontal(x) || checkVertical(y) || checkLeftToRightDiagonal() || checkRightToLeftDiagonal()) {
			return true;
		}
		else return false;
	}

	public boolean isBingo() {
		return isBingo;
	}

	public void setBingo(boolean isBingo) {
		this.isBingo = isBingo;
	}

	public String markNum(int num){
		//TODO Implement
		for(int i = 0; i < numbers.length; i++) {
			for(int j = 0; j < numbers[i].length; j++) {
				Number current = numbers[i][j];
				if(current.getValue() == num) {
					if(current.isChecked()) {
						return num + " sebelumnya sudah tersilang";
					}
					else {
						current.setChecked(true);
						if(isBingo(current.getX(), current.getY())) {
							setBingo(true);
							return "BINGO!";
						}
						else {
							return num + " tersilang";
						}
					}
				}
			}
		}
		return "Kartu tidak memiliki angka " + num;
	}	
	
	public String info(){
		//TODO Implement
		String res = "";
		for(int i = 0; i < numbers.length; i++) {
			for (int j = 0; j < numbers[i].length; j++) {
				Number current = numbers[i][j];
				if(current.isChecked()) {
					res += "| X  ";
				}
				else {
					res += "| " + current.getValue() + " ";
				}
			}
			res += "|\n";
		}
		return res.substring(0, res.length()-1);
	}
	
	public void restart(){
		//TODO Implement
		for(int i = 0; i < numbers.length; i++) {
			for (int j = 0; j < numbers[i].length; j++) {
				Number current = numbers[i][j];
				if(current.isChecked()) {
					current.setChecked(false);
				}
			}
		}
		System.out.println("Mulligan!");
	}
	

}
