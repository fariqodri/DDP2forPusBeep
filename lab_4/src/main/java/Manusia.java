//Author: Qori
//Date: 26 Februari 2018

public class Manusia {
    private final String nama;
    private final int umur;
    private int uang;
    private double kebahagiaan;
    private final double MAX_HAPPINESS = 100.0;
    private final double MIN_HAPPINESS = 0.0;

    public Manusia(String nama, int umur) {
        this.nama = nama;
        this.umur = umur;
        this.kebahagiaan = 50.0;
        this.uang = 50000;
    }

    public Manusia(String nama, int umur, int uang) {
        this.nama = nama;
        this.umur = umur;
        this.uang = uang;
        this.kebahagiaan = 50.0;
    }

    public String getNama() {
        return nama;
    }

    public int getUmur() {
        return umur;
    }


    public int getUang() {
        return uang;
    }

    public void setUang(int uang) {
        this.uang = uang;
    }

    public double getKebahagiaan() {
        return kebahagiaan;
    }

    public void setKebahagiaan(double kebahagiaan) {
        this.kebahagiaan = kebahagiaan;
    }

    /*
    Mengatur kebahagiaan dari ygDiganti ketika method beriUang dipanggil
     */
    public void adjustKebahagiaan(Manusia ygDiganti, double perubahan) {
        //SET KEBAHAGIAAN OBJECT
        ygDiganti.setKebahagiaan(ygDiganti.getKebahagiaan() + perubahan);
        if(ygDiganti.getKebahagiaan() > MAX_HAPPINESS) {
            ygDiganti.setKebahagiaan(MAX_HAPPINESS);
        }
        else if(this.kebahagiaan < MIN_HAPPINESS) {
            ygDiganti.setKebahagiaan(MIN_HAPPINESS);
        }
    }

    /*

     */
    public void beriUang(Manusia penerima) {
        String namaPenerima = penerima.getNama();
        int ascii = 0;
        //Hitung ascii
        for(int i = 0; i < namaPenerima.length(); i++) {
            ascii += (int) namaPenerima.charAt(i);
        }
        int uangDiberikan = ascii * 100;
        int temp = this.uang - uangDiberikan; //Jumlah uang pemberi setelah dikurangi uang yg diberikan (temporary)

        //Jika uang pemberi < uang diberikan
        if(temp < 0) {
            System.out.println(this.nama + " ingin memberi uang kepada " + namaPenerima + " namun tidak memiliki cukup uang :\'(");
        }
        //Jika uang pemberi > uang diberikan
        else {
            double perubahan = (double) (uangDiberikan)/6000;
            adjustKebahagiaan(penerima, perubahan);     //Mengubah kebahagiaan dari penerima duid
            adjustKebahagiaan(this, perubahan); //Mengubah kebahagiaan dari pemberi duid
            this.uang = temp;   //Assign temp ke jumlah uang yg sesungguhnya
            penerima.setUang(penerima.getUang() + uangDiberikan);   //Ubah jumlah uang dari penerima
            System.out.println(this.nama + " memberi uang sebanyak " + uangDiberikan + " kepada " + penerima.getNama()
            + ", mereka berdua senang :D");
        }
    }

    /*
    Sama kayak beriUang yg diatas
     */
    public void beriUang(Manusia penerima, int jumlah) {
        String namaPenerima = penerima.getNama();
        int temp = this.uang - jumlah;
        if(temp < 0) {
            System.out.println(this.nama + " ingin memberi uang kepada " + namaPenerima + " namun tidak memiliki cukup uang :\'(");
        }
        else {
            this.uang = temp;
            penerima.setUang(penerima.getUang() + jumlah);
            double perubahan = (double) jumlah / 6000;
            adjustKebahagiaan(this, perubahan); //Mengubah kebahagiaan dari pemberi duid
            adjustKebahagiaan(penerima, perubahan);    //Mengubah kebahagiaan dari penerima duid
            System.out.println(this.nama + " memberi uang sebanyak " + jumlah + " kepada " + penerima.getNama()
                    + ", mereka berdua senang :D");
        }
    }

    /*
    Code sesuai spesifikasi soal
     */
    public void bekerja(int durasi, int bebanKerja) {
        if(this.umur < 18) {
            System.out.println(this.nama + " belum boleh bekerja karena masih dibawah umur D:");
        }
        else {
            int bebanTotal = durasi * bebanKerja;
            int pendapatan = bebanTotal * 10000;
            if(bebanTotal <= this.kebahagiaan) {
                System.out.println(this.nama + " bekerja full time, total pendapatan : " + pendapatan);
            }
            else {
                int durasiBaru = (int) this.kebahagiaan / bebanKerja;
                bebanTotal = durasiBaru * bebanKerja;
                pendapatan = bebanTotal * 10000;
                System.out.println(this.nama + " tidak bekerja secara full time karena sudah terlalu lelah, total pendapatan : " + pendapatan);
            }
            //Ubah uang dan kebahagiaan berdasarkan spesifikasi soal
            this.uang += pendapatan;
            adjustKebahagiaan(this, -bebanTotal);
        }
    }

    /*
    Code sesuai spesifikasi soal
     */
    public void rekreasi(String namaTempat) {
        int biaya = namaTempat.length() * 10000;
        int temp = this.uang - biaya;
        if(temp >= 0) {
            this.uang = temp;
            adjustKebahagiaan(this, namaTempat.length());    //Ubah kebahagiaan dari orang yg berekreasi
            System.out.println(this.nama + " berekreasi di " + namaTempat + ", " + this.nama + " senang :)");
        }
        else {
            System.out.println(this.nama + " tidak mempunyai cukup uang untuk berekreasi di " + namaTempat + " :(");
        }
    }

    /*
    Code sesuai spesifikasi soal
     */
    public void sakit(String namaPenyakit) {
        adjustKebahagiaan(this, -namaPenyakit.length());    //Ubah kebahagiaan dari orang yg berekreasi
        System.out.println(this.nama + " terkena penyakit " + namaPenyakit + " :0");
    }

    @Override
    public String toString() {
        return "Nama\t\t: " + this.nama + "\nUmur\t\t: " + this.umur + "\nUang\t\t: " + this.uang + "\nKebahagiaan\t: " +
                this.kebahagiaan;
    }
}
